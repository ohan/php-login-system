<?php
	include('lib/app.php');
	$config = $_SESSION['config'];
	$cities = $config['cities'];
	$hobbies = $config['hobbies'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>create</title>
</head>
<body>
<nav>
	<li><a href="index.php">Home</a></li>
</nav>
<form action="add.php" method="post" enctype="multipart/form-data">
	<fieldset>
		<legend>Registration: </legend>
		<!-- user type: only ADMIN can view -->
		<?php if(get_user_type() == 'ADMIN'):?>
		<div>
			<label for="user_type">user type</label>
			<select name="user_type" id="user_type">
				<option  selected="selected" value="GENERAL">GENERAL</option>
				<option  value="ADMIN">ADMIN</option>
			</select>
		</div>
		<?php endif?>
		<!-- name -->
		<div>
			<label for="txtFullName">Full name</label>
			<input type="text" name="full_name" id="txtFullName"value="">
		</div>
		<!-- email -->
		<div>
			<label for="email">Email</label>
			<input type="text" name="email" id="email"value="">
		</div>
		<!-- password -->
		<div>
			<label for="password">Password</label>
			<input type="password" name="password" id="password"value="">
		</div>

		<!-- gender -->
		<div>
			<label>Gender</label>
			<input type="Radio" name="gender" id="optGender1" value="male" checked>
			<label for="optGender1">Male</label>

			<input type="Radio" name="gender" id="optGender2" value="female">
			<label for="optGender2">Female</label>
		</div>

		<!-- city -->
		<div>
			<label for="city">Select City</label>
			<select name="city" id="city">
				<option disabled="disabled" selected="selected" value="NA">Select A city</option>
				<?php foreach($cities as $city):?>
					<option value="<?php echo $city?>"><?php echo $city?></option>
				<?php endforeach;?>
			</select>
		</div>
		<!-- hobbies -->
		<div>
			<label >Select Your Hobbies</label>
			<?php foreach($hobbies as $hobby_value => $hobby_label): ?>
				<p>
					<input type="checkbox" name="hobbies[]" value="<?php echo $hobby_value; ?>" /> <?php echo $hobby_label?>
				</p>
			<?php endforeach;?>
		</div>

		<!-- about me -->
		<div>
			<label for="about_me">About me</label>
			<textarea name="about_me" id="about_me"></textarea>
		</div>

		<!-- profile img -->
		<div>
			<label for="photo"> Upload Your Photo</label>
			<input type="file" name="photo">
		</div>
		
		
		<!-- submit -->
		<input type="submit" value="Save Info">
	</fieldset>
</form>

</body>
</html>