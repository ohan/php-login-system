<?php
require_once('lib/app.php');

if(!user_loggedin()){
	header('location: login.php');
}
$user = $_SESSION['user'];
?>


<nav>
	<li><a href="index.php">Home</a></li>
	<li><a href="delete.php?id=<?php echo $user['id'];?>">Delete My account</a></li>
	<li><a href="edit.php?id=<?php echo $user['id'] ?>">Edit my profile</a></li>
	<li><a href="logout.php">Logout</a></li>
</nav>
<h1>Welcome! <?php echo $user['full_name'];?></h1>

<p><b>User Type:</b> <?php echo $user['user_type']; ?></p>
<p><b>Email:</b> <?php echo $user['email']; ?></p>
<p><b>Password:</b> <?php echo $user['password']; ?></p>
<p><b>Gender:</b> <?php echo $user['gender']; ?></p>
<p><b>City:</b> <?php echo $user['city']; ?></p>
<p><b>Hobbies:</b> <?php echo $user['hobbies']; ?></p>
<p><b>about me:</b> <?php echo $user['about_me']; ?></p>