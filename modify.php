<?php
require_once('lib/app.php');

$id = $_GET['id'];

//authorization
if(user_loggedin() AND (intval($_SESSION['user']['id']) == $id OR get_user_type() == 'ADMIN')){
	//can proceed
}else{
	$_SESSION['msg_error'] = 'You do not have permission to modify the data';
	header('location: index.php');
	exit();
}


//intialize data variables
$full_name = '';
$email = '';
$gender = '';
$city = '';
$hobbies = '';
$about_me = '';
$user_type = 'GENERAL';
//hold full_name
if(isset($_POST['full_name']))
	$full_name = $_POST['full_name'];
//hold email
if(isset($_POST['email']))
	$email = $_POST['email'];
//hold gender
if(isset($_POST['gender']))
	$gender = $_POST['gender'];
//hold city
if(isset($_POST['city']))
	$city = $_POST['city'];
//hold hobbies
if(isset($_POST['hobbies']) && !empty($_POST['hobbies']))
	$hobbies = implode(',',$_POST['hobbies']);
//hold about_me
if(isset($_POST['about_me']))
	$about_me = $_POST['about_me'];
if(isset($_POST['user_type']))
	$user_type = $_POST['user_type'];

$photo_tmp = $_FILES['photo']['tmp_name'];
$photo_id = uniqid();
$photo_type = $_FILES['photo']['type'];
$photo_path = 'photo/'.$photo_id;
move_uploaded_file($photo_tmp, $photo_path);

$query = "UPDATE user SET full_name='".$full_name.
						"',"."email='".$email.
						"',"."gender='".$gender.
						"',"."city='".$city.
						"',"."hobbies='".$hobbies.
						"',"."about_me='".$about_me.
						"',"."user_type='".$user_type.
							"' WHERE id=".$id;
// //dd($query);

$success = mysqli_query($link,$query);
if($success){
	$_SESSION['msg_success'] = "Data is successfully updated";
}else{
	$_SESSION['msg_error'] = "Failed to update data";
}

header('location: index.php');
