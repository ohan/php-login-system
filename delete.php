<?php
require_once('lib/app.php');

$id = $_GET['id'];

//authorization
if(user_loggedin() AND (intval($_SESSION['user']['id']) == $id OR get_user_type() == 'ADMIN')){
	//can proceed
}else{
	$_SESSION['msg_error'] = 'You do not have permission to modify the data';
	header('location: index.php');
	exit();
}

if(!is_null($id)){

	$query = "SELECT * FROM user WHERE id=".$id;
	$result = mysqli_query($link, $query);
	$user = array();
	$user = mysqli_fetch_assoc($result);
	
	$query = "DELETE FROM user WHERE id=".$id;

	$success = mysqli_query($link,$query);
	
	if($success){
		//delete the photo from photo directory
		unlink($user['photo_path']);
		$_SESSION['msg_success'] = "successfully deleted item";
		//if a user delete himself, destroy his session
		if($_SESSION['user']['id'] == $id){
			unset($_SESSION['user_loggedin']);
			unset($_SESSION['user']);
		}
		
	}else{
		$_SESSION['msg_error'] = "can't be deleted";
	}
}

header('location: index.php');