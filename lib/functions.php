<?php

function user_loggedin(){
	if(isset($_SESSION['user_loggedin']) AND $_SESSION['user_loggedin'] == true){
		return true;
	}else{
		return false;
	}
}

function get_user_type(){
	if(isset($_SESSION['user'])){
		return $_SESSION['user']['user_type'];
	}else{
		return false;
	}
}

function show_user_info(){
	if(user_loggedin()){
		$user = $_SESSION['user'];
		echo $user['full_name'].' ('.$user['email'].') '.'['.$user['user_type'].']';
	}else{
		echo 'Anonymous User';
	}
}